module.exports = function(api) {
    api.cache(true);
 
    const isDev = process.env.NODE_ENV === 'development';

    const presets = ["@babel/env", "@babel/preset-react"];

    const plugins = isDev ? ["react-refresh/babel"] : [];

    return {
        presets,
        plugins
    };
}