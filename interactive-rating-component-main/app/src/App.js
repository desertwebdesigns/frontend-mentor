import React, { useState } from 'react';

import star from './assets/images/icon-star.svg';
import thankyouimg from './assets/images/illustration-thank-you.svg';

import "./assets/css/style.scss";

export default function App() {
  const [rating, setRating] = useState(null)
  const [formComplete, setFormComplete] = useState(false)

  const ratingOptions = [1,2,3,4,5];

  const form = 
    <div id='form'>
      <img src={star} alt='star-icon' className="star" />
      <h1>How did we do?</h1>
      <p>Please let us know how we did with your support request. All feedback is appreciated to help us improve our offering!</p>
      <ul>
        {
          ratingOptions.map((option, i) => {
            return (
              <li
                key={i}
                className={ parseInt(rating) === (i + 1) ? "selected" : "" }
                onClick={(e) => setRating(e.target.innerHTML)}
              >
                {option}
              </li>
            )
          })
        }
      </ul>
      <button onClick={() => setFormComplete(true)}>Submit</button>
    </div>


  const thankyou = 
    <div id="thankyou">
      <img src={thankyouimg} alt="thank-you" id="thankyouimage" />
      <p className="rating">You selected {rating} out of 5</p>
      <h1>Thank you!</h1>
      <p>We appreciate you taking the time to give a rating. If you ever need more support, don't hesitate to get in touch!</p>
    </div>

  return (
    <>
      <main className="container">
        { 
          !formComplete 
          ?
          form
          : thankyou 
        }
      </main>
      <footer className="attribution">
        Challenge by <a href="https://www.frontendmentor.io?ref=challenge" target="_blank">Frontend Mentor</a>. 
        Coded by <a href="https://desertwebdesigns.com">Desert Web Designs</a>.
      </footer>
    </>
  );
}