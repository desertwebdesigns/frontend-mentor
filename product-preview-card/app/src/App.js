import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartShopping } from '@fortawesome/free-solid-svg-icons'


import "./assets/css/style.scss";

export default function App() {
  return (
    <>
      <main className="container">
        <aside id="image"></aside>
        <section id="product">
          <span className="category">Perfume</span>
          <h1>Gabrielle Essence Eau De Parfum</h1>
          <p className="description">A floral, solar and voluptuous interpretation composed by Olivier Polge, Perfumer-Creator for the House of CHANEL.</p>
          <div className="pricing_info">
            <h2 className="price">$149.99</h2>
            <p className="orig_price">$169.99</p>
          </div>
          <button><FontAwesomeIcon icon={faCartShopping} /> Add to Cart</button>
        </section>
      </main>
      <footer className="attribution">
        Challenge by <a href="https://www.frontendmentor.io?ref=challenge" target="_blank">Frontend Mentor</a>. 
        Coded by <a href="https://desertwebdesigns.com">Desert Web Designs</a>.
      </footer>
    </>
  );
}