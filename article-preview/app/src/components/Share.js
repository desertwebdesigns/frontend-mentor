import React from 'react';
import ShareIcon from '../assets/images/icon-share.svg';

export default function Share({...props}) {
    return (
        <a href="#" className="text-hide d-flex rounded-circle" {...props}>
            <ShareIcon className="m-auto" />
        </a>
    );
}