import React from "react";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
import '../assets/css/desktop.popover.css';

import FacebookIcon from '../assets/images/icon-facebook.svg';
import PinterestIcon from '../assets/images/icon-pinterest.svg';
import TwitterIcon from '../assets/images/icon-twitter.svg';
import Share from './Share';

export default function DesktopPopover() {    
    const renderTooltip = (props) => (
        <Tooltip id="share" {...props}>
            <span>Share</span>
            <FacebookIcon />
            <TwitterIcon />
            <PinterestIcon />
        </Tooltip>
      );    

      return (
        <OverlayTrigger
            trigger="click"
            placement="top"
            overlay={renderTooltip}
            rootClose={true}
        >
            <Share />
        </OverlayTrigger>
    );
}