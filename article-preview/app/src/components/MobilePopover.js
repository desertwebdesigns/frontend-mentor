import React from "react";
import Share from './Share';

export default function MobilePopover({onClick}) {
    return <Share onClick={onClick} />
}