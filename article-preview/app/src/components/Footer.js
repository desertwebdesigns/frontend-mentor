import React from "react";

export default function Footer() {
    return (
        <footer className="attribution mx-auto">
            Challenge by <a href="https://www.frontendmentor.io?ref=challenge" target="_blank">Frontend Mentor</a>. 
            Coded by <a href="http://www.desertwebdesigns.com">Desert Web Designs</a>.
        </footer>
    );
}