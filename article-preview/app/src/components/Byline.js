import React, { useState } from "react";
import Row from 'react-bootstrap/Row'; 
import Col from 'react-bootstrap/Col';
import { useMediaQuery } from 'react-responsive';
import DesktopPopover from "./DesktopPopover";
import MobilePopover from "./MobilePopover";
import breakpoints from "../utilities/breakpoints";

import '../assets/css/mobile.popover.css';
import FacebookIcon from '../assets/images/icon-facebook.svg';
import PinterestIcon from '../assets/images/icon-pinterest.svg';
import TwitterIcon from '../assets/images/icon-twitter.svg';
import Share from "./Share";
import avatar from '../assets/images/avatar-michelle.jpg';

export default function Byline() {
    const isPhone = useMediaQuery({query: breakpoints.phone});
    const isDesktop = useMediaQuery({query: breakpoints.desktop});

    const [clicked, setClicked] = useState(false);

    const toggleClicked = () => {
        setClicked(!clicked);
    }

    if (isPhone && clicked) {
        return (
            <Row noGutters={true} id="byline" className="p-4 p-lg-0 clicked align-items-center">
                <Col xs={10} className="d-flex justify-content-center pr-5">
                    <span>Share</span>
                    <FacebookIcon />
                    <TwitterIcon />
                    <PinterestIcon />
                </Col>
                <Col xs={2} className="share">
                    <Share onClick={toggleClicked} />
                </Col>
            </Row>
        );
    } else {
        return (
            <Row noGutters={true} id="byline" className="px-5 pb-4 p-lg-0">
                <Col xs={3} lg={2} className="avatar">
                    <img src={ avatar } alt="Michelle Appleton" className="rounded-circle" />
                </Col>
                <Col xs={7} lg={8} className="details p-0 d-flex flex-column justify-content-lg-end">
                    <span className="author">Michelle Appleton</span>
                    <span className="date">28 Jun 2020</span>
                </Col>
                <Col xs={2} lg={2} className="share m-auto d-flex flex-row-reverse">
                    { isDesktop && <DesktopPopover />}
                    { isPhone && <MobilePopover onClick={toggleClicked} /> }
                </Col>
            </Row>
        );
    }
}