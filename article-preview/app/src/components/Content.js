import React from "react";
import Row from 'react-bootstrap/Row'; 
import Col from 'react-bootstrap/Col'; 
import Byline from './Byline';
import drawers from '../assets/images/drawers.jpg';

export default function Content() {
    return (
        <Row id="content" noGutters={true} className="bg-white">
            <Col className="photo" lg={5}>
                <img src={ drawers } alt="Drawers" className="img-fluid" />
            </Col>
            <Col lg={7} className="bg-white d-flex flex-column justify-content-between ml-lg-n5 pl-lg-5 py-lg-4 mt-n5 mt-lg-0">
                <Row noGutters={true} className="title-preview px-5 pt-5 pb-3 p-lg-0">
                    <h4 className="m-lg-0 p-lg-0">Shift the overall look and feel by adding these wonderful 
                touches to furniture in your home</h4>
                </Row>
                <Row noGutters={true} className="content-preview px-5 pt-2 pb-4 p-lg-0">
                    <p className="m-lg-0 p-lg-0">Ever been in a room and felt like something was missing? Perhaps 
                it felt slightly bare and uninviting. I’ve got some simple tips 
                to help you make any room feel complete.</p>
                </Row>
                <Byline />
            </Col>
        </Row>
    )
}

