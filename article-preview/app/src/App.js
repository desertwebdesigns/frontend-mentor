import React from "react";
import Row from 'react-bootstrap/Row'; 
import Col from 'react-bootstrap/Col';
import Content from './components/Content';
import Footer from './components/Footer'; 

// Importing the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import "./assets/css/style.css";

export default function App() {
  return(
    <Row className="justify-content-center" noGutters={true}>
      <Col xs={10} md={8} xl={6}>
        <Row id="preview">
          <Content />
          <Footer />
        </Row>
      </Col>
    </Row>
  );
}