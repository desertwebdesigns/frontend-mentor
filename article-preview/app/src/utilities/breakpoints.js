const breakpoints = {
    desktop: '(min-width: 1200px)',
    phone: '(max-width: 767px)',
};

export default breakpoints;