const path = require("path");
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  module: {
    rules: [
        {
            test: /\.css$/,
            use: [
              { loader: "style-loader" },
              { loader: "css-loader" },
            ]
        },
    ]
  },
  devServer: {
    client: {
      overlay: false,
    },
    static: [
        {
            directory: path.join(__dirname, "design"),
            publicPath: "/design",
            serveIndex: true
        }
    ], 
    port: 3000,
    hot: true
  },
  plugins: [new ReactRefreshWebpackPlugin()]
});