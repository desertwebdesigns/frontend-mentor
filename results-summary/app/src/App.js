import React, {useState} from 'react';
import Result from './components/Result';
import Summary from './components/Summary';

import "./assets/css/style.scss";

export default function App() {
  return (
    <>
      <main className="container">
        <Result />
        <Summary />
      </main>
      <footer className="attribution">
        Challenge by <a href="https://www.frontendmentor.io?ref=challenge" target="_blank">Frontend Mentor</a>. 
        Coded by <a href="https://desertwebdesigns.com">Desert Web Designs</a>.
      </footer>
    </>
  );
}