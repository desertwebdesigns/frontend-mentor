import React from 'react';

const Result = () => {
    return (
        <aside>
            <h1>Your Result</h1>
            <p id="score"><span>76</span> of 100</p>
            <h2>Great</h2>
            <p>You scored higher than 65% of the people who have taken these tests.</p>
        </aside>
    )
}

export default Result