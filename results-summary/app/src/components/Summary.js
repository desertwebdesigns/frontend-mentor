import React from 'react';

import reaction from "../assets/images/icon-reaction.svg"
import memory from "../assets/images/icon-memory.svg"
import verbal from "../assets/images/icon-verbal.svg"
import visual from "../assets/images/icon-visual.svg"

import data from "../data.json";

const Summary = () => {
    return (
        <section id="summary">
            <h3>Summary</h3>
            <ul id="breakdown">
                {
                    data.results.map(result => {
                        return (
                            <li id={result.category.toLowerCase()} key={result.category}>
                                <img src={result.icon} alt="Reaction icon"/>
                                <p className="category">{result.category}</p>
                                <p className="results"><span className="score">{result.score}</span> / 100</p>
                            </li>
                        )
                    })
                }
            </ul>
            <button>Continue</button>
        </section>
    )
}

export default Summary