import React, {useState} from 'react';

import qr from "./assets/images/image-qr-code.png";

import "./assets/css/style.scss";

export default function App() {
  return (
    <>
      <main className="container">
        <img src={qr} alt="QR Code" />
        <h1>Improve your front-end skills by building projects</h1>
        <p>Scan the QR code to visit Frontend Mentor and take your coding skills to the next level</p>
      </main>
    </>
  );
}